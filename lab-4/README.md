#Design some performance tests that can run as part of our pipeline to test capacity in our system

When you are building a product you normally have an intended goal in mind. You might have 15 users, you might have 15 million. The decisions you make in your system design should be influenced by the realties of how it is being used.

The first step quite often in this process is describing the load of your system. Load can be described with using what is called `load paramters`  The best paramaters depends on the system itself. It might be request per seconds for a web server or the number of writes/sec for a database or the time it takes for an update to flow through your whole system. 

Once you have identified your `load paramters` you  have a KPI you can monitor. Only then can you start considering scalability and reliability. What happens if the load on my web server doubles? What happens if our system has to suddenly ingest five times as many messages?


We can describe the performance of our system against two contrasting view points. Some sample questions have been added against these 2 primary load investigations for an example web service like the read through view-cache service

1. When we increase the load parameter and keep system resources the same (CPU, memory, benadwidth etc) unchanged, how is the performance of your system affected? 
 - How is response latency affected (mean, 95th and 99th)
 - Are the primitive resources we have (CPU, Memory, network and file IO) drastically spiking compared to the relationship of the increase in load.
2. When we increase the load parameter, how much do you need to increase the resources of your system to keep performance unchanged? 
 - Does our system allow for easy scaling and sharding, do we have certain bottlenecks in our design
 - Does our system have the capability to scale up and down to keep costs under control






## Onto the lab itself

Errata: We use gatling for this lab. There are numerous load testing tools out there. We also execute the the load test in a single container running on the same cluster. We advise against this. Your load test is likely being constrained by the network and CPU throughput of the single container. Running your load test in parallel is relatively easy and is described here. Alternatively you can use  BIG FAT INSTANCE with hight CPU and high Network IO to execute the test. We would also recommend that you deploy your load tests onto a different cluster to provide isolation and accuracy. 


### 1. Create the gatling test. 
Gatling is a JVM based load test which uses a DSL which your write in Scala to define your test. A simple load test looks like this
```scala

package loadtests

import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import scala.concurrent.duration._

class ViewCacheServiceSimulation extends Simulation {
  val url = "http://35.184.138.147/get-data?key=1234" //terrible terrible hardcing this
  val scn: ScenarioBuilder = scenario("view-cache-simulation")
    .exec(http("view-cache-simulation")
      .get(url)
      .check(status is 200)
    )

  setUp(
    scn.inject(
      rampUsersPerSec (0) to 425 during (1 second),
      constantUsersPerSec(425) during (5 minutes)
    )
  )
  .assertions(forAll.successfulRequests.percent.gte(99.9))
  .assertions(forAll.requestsPerSec.between(395, 500))
}


```

We are calling the same URL 425 times per second (immediatlly after 1 second) and then running that constantly for 5 minutes. We are ensuring that 99.9 percent of our requests returns a HTPP 200 status. We are also ensuring that we are continuously being able to serve bewteen 395 and 500 requests per second.


This test has a problem. It uses 1 url. Our cache hit should be super close to 100%. This test does not test a breadth of endpoints and thus not scaling in memory or if the underlying service which is invoked by view-cache is scalable.


We need to make sure that we have some cache misses. We can do this by supplying a function for our load test query string.

```scala

 val scn: ScenarioBuilder = scenario("view-cache-simulation")
    .exec(http("view-cache-simulation")
      .get(url)
      .queryParam("key", _ => randomNumGen.nextInt(500).toString))
      .check(status is 200)
    )

```


### 2. Having the test run as part of your deployment pipeline

(You can pick up the ./performance/ directory from within the services directory. The load test itself can be found at ./services/performance/src/test/scala/loadtests/ViewCacheServiceSimulation.scala)  

To execute the performance test I have added a new stage to the gitlab pipeline called performance. Running it is as simple as 
```yaml

performance-tests:
  image: gradle
  stage: performance
    
  script:
    - cd performance
    - gradle gatlingRun-loadtests.ViewCacheServiceSimulation


```


When should this stage be execute? How often?


### Exercises:


1. Have this performance test execute in your pipeline after your have deployed.



## Concluding Lab 4

You are now finished with Lab 4 in this lab we learned
* How to construct a simple load test in gatling
* How to execute this load test as part of your gitlab pipeline


In the next lab, we will look at [Introduce a traffic-shifting approach to production deployments](../lab-5/README.md)
