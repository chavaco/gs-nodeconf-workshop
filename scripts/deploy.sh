#!/bin/sh
VERSION=$(echo $CI_COMMIT_REF_NAME | awk -F"_" '{print $1}')
gcloud config set project ${GCLOUD_PROJECT_ID}
gcloud container clusters get-credentials ${GCLOUD_CLUSTER_NAME} --zone us-central1-a --project ${GCLOUD_PROJECT_ID}
gcloud components install kubectl
sed -i "s|FULL_REPOSITORY_NAME|us.gcr.io/${GCLOUD_PROJECT_ID}/${APP_NAME}:${VERSION}|" "./services/$APP_NAME/$APP_NAME-deployment.yaml"
kubectl apply -f "$APP_NAME/$APP_NAME-deployment.yaml"
kubectl apply -f "$APP_NAME/$APP_NAME-gateway.yaml"