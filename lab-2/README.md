# Lab 2 - Deploying Services Using Gitlab

In our previous lab, we walked through the steps necessary to get a Kubernetes running and integrated with Gitlab. In this section we 
will actually deploy some nodejs services in the cloud. 

## Service Architecture 

The below figure shows the target architecture for the system.


![alt text](nodeconf-topology.png "K8s cluster detail")

There are 3 important services

* gui-app-svc - The UI for the application
* view-cache-svc - A simple read through caching service, which communicates with the backend
* back-end-svc - A simple back end service which serves some information about books

You can browse the code for each of these services in the 'services' directory. Each supplies as Dockerfile which 
will build a packaged container with the service functionality. 

## Part 1 - Building and Testing

To build and test services in gitlab, we need to create a `.gitlab-ci.yml` in the root of the project. One has been partially provided for this exercise.

Note that a sample yml has been provided that will build the front-end service. 

To see the results of running a pipeline, go to your gitlab.com project ui > ci/cd > pipelines and click the button to run a pipeline
on the master branch. 

The results should look something like the below

![alt text](nodeconf-pipeline.png "sample pipeline run")

Now, lets work on getting the build and test stages fully operational. 

### Task 1 - Create Build Jobs For Other Services

Your first task is to create build jobs for the back-end and view-cache services. 
* These jobs should simply run `npm install` and 
then `npm build`.
* Each job should execute in the `build` lifecycle stage 
* Each job should save the output of the `build` and `node_modules`

### Task 2 - Create Test Jobs For All Services

Ok, now we're getting warmed up, lets try create a few new jobs to execute the unit tests for our project. 

* Create test jobs for the back-end, gui-app and view-cache services
* These jobs should execute in the `test` stage
* Each job should run `npm install`  and `npm run test`

Good, now if we run a pipeline, all services should run their test, and following this run their build. 

## Part 2 - Write Deployment and Gateway Descriptors


### Deployments
Next, we're going to deploy our services to Kubernetes. First, lets cover the components of a deployment in GKE. 
There are two main components, the *Service* and the *Deployment*.

* The Deployment specifies the number of replicas, the container version, image and container port
* The Service specifies a port for the service which will route traffic to the container ports

An example is annotated below 

```yaml
apiVersion: apps/v1 #apps is the most common API group in Kubernetes, with many core objects being drawn from it and v1.
 # It includes functionality related to running applications on Kubernetes, like Deployments, RollingUpdates, and ReplicaSets.
kind: Deployment # The Resource type see https://kubernetes.io/docs/concepts/workloads/controllers/deployment/
metadata:
  name: sample-deployment # Name of the resource
  labels:
    app: sample
spec:
  replicas: 2 # number of pods to run
  selector:
    matchLabels:
      app: sample
  template:
    metadata:
      labels:
        app: sample
    spec:
      containers:
      - name: sample
        image: "FULL_REPOSITORY_NAME" #this should always be "FULL_REPOSITORY_NAME", it will be replaced by the deployment scripts
        ports:
        - containerPort: 81 # container port
---
apiVersion: v1
kind: Service # see https://kubernetes.io/docs/concepts/services-networking/service/
metadata:
  name: sample-svc # name of the resource, connects with endpoints
  labels:
    app: sample
spec:
  ports:
  - port: 81 # service port
    name: http
  selector:
    app: sample # seach space for pods to include in the service

```

#### Task 3 - Create the deployments

Create a file in each of the services listed in the services directory called ${service-name}-deployment.yaml, e.g back-end-deployment.yaml 

Using the example above as a starting point fill out the contents of the yaml. You should include both the Service and Deployment

* The back end service should run on port 4001
* The gui should run on 5000
* The view-cache should run on 4000

### Gateways

Now we have some pods running our service we'll need to expose them to the outside world. This is done using the
Gateway and Virtual Service resources. An example is annotated below

```yaml
apiVersion: networking.istio.io/v1alpha3
kind: Gateway # The gateway resource
metadata:
  name: sample-gateway # name of the resource
spec:
  selector:
    istio: ingressgateway # use istio default controller
  servers:
  - port:
      number: 80 # Globally expose the service on port 80
      name: http
      protocol: HTTP
    hosts:
    - "*"
---
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService # The virtual service resource
metadata:
  name: sample
spec:
  hosts:
  - "*"
  gateways:
  - sample-gateway # refers to the resource name from the gateway section
  http:
  - match:
    - uri:
        prefix: /sample-prefix # This route must be available on the service
    route:
      - destination:
          host: sample-service
          port:
            number: 10000 # The port of the service, defined in the deployment
  - match:
    - uri:
        prefix: /sample-help
    rewrite:
      uri: /help # certain prefixes may conflict in a global namespace, rewrites can help
    route:
      - destination:
          host: sample-service
          port:
            number: 10000
```

#### Task 4 - Create the Gateways

Create some gateways for view-cache and gui service. Note: we do not want a gateway for the back-end service, this service is private! 

* The view-cache gateway should map the /get-data endpoint from port 80 globally to the container port of the service
* The gui-app should map the /static route and should rewrite /gui to / on port 80 routing to the container port of the service. 




## Part 3 - Release and Deploy 

Next, we're going to write a job to release our service to the Google Container Registry ( GCR ). First 
lets start by setting up some environment variables for our runners. In the Settings > CI/CD > Variables section
of the gitlab project
* Create a variable called `GCLOUD_SERVICE_KEY`. Populate it with the .json format key of the gitlab-ci service account. 
You can view this in the IAM > Service Accounts section of the google cloud console. 
* Create a variable called `GCLOUD_PROJECT_ID`, you can find your project id by running `gcloud config list
`
* Finally we need a variable for the cluster `GCLOUD_CLUSTER_NAME`. Populate this with gs-nodeconf-ci.

Next take a look in `scripts` section of the project. There are some helpful scripts that should simplify the job of 
deploying and releasing. Here's a quick overview of their purpose. 

* `before-release-or-deploy.sh` - Authenticates with GCR so containers can be created / deployed
* `release.sh` - Builds and pushes a container using the CI commit ref as a version
* `deploy.sh` - Sets up kubectl. It also does some string manipulation to the service deployment descriptor to ensure the 
correct version of the service is deployed.  

### Task 5 - Add Release Jobs To Gitlab

Your next task is going to be to add some jobs to gitlab to release the project. For each service create a job which

* Uses the image `docker:latest`
* Operates in the `release` stage
* Sets a variable `DOCKER_DRIVER: overlay`
* Depends on the build for the service succeeding
* Executes the `before-release-or-deploy.sh` script in the `before_script` section
* Executes the `release.sh` script in the `script` section
* Operates *only* on tags

You can create manage tags in the gitlab.com Repository section, but it also may be wise to leave the operate on tags only 
directive until last. 

### Task 6 - Add Deploy Jobs to Gitlab 

Finally, we need to create the gitlab job to deploy our released container to GKE. Create a job for each service which 

* Uses the image `google/cloud-sdk:alpine`
* Operates in the `deploy` stage
* Executes the `before-release-or-deploy.sh` script in the `before_script` section
* Executes the `deploy.sh` script in the `script` section
* Operates *only* on tags 

Tips: 
* Check out yaml list merges as a way to avoid repeating yourself in the stages! 
* It's possible to locally test changes using the [gitlab's guide]()
 


## Concluding Lab 2 

You are now finished with Lab 2, in this lab we learned
* How to build pipelines in gitlab ci 
* About Deployment, Service, Virtual Service and Gateway Resources
* How to deploy services on Kubernetes 

In the next lab, we will look at [Monitoring Live Services](../lab-3/README.md)
