Gatling-Performance-Tests
=========================

Gatling tests using the gatling-maven-plugin.

To test it out, simply execute the following command:

    $mvn gatling:test -Dgatling.simulationClass=loadtests.SamplePostRequest

or simply:

    $mvn gatling:test
