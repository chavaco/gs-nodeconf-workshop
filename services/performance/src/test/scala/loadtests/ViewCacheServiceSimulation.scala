package loadtests

import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import scala.concurrent.duration._
import scala.util.Random

class ViewCacheServiceSimulation extends Simulation {

  val randomNumGen = new Random()
  val url = "http://35.184.138.147/get-data"
  val scn: ScenarioBuilder = scenario("view-cache-simulation")
    .exec(http("view-cache-simulation")
      .get(url)
      .queryParam("key", _ => randomNumGen.nextInt(500).toString)
      .check(status is 200)
    )

  setUp(
    scn.inject(
      rampUsersPerSec (0) to 425 during (1 second),
      constantUsersPerSec(425) during (5 minutes)
    )
  )
  .assertions(forAll.successfulRequests.percent.gte(99.9))
  .assertions(forAll.requestsPerSec.between(395, 500))
}
